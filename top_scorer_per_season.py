from csv import DictReader
from matplotlib import pyplot as plt
import ids_of_match_by_season

def get_scorer(file_path, ids_of_match_by_season):
    batsman = {}
    with open(file_path, 'rt') as csv_file:
        deliveries = DictReader(csv_file)
        for delivery in deliveries:
            if delivery['match_id'] in ids_of_match_by_season:
                if delivery['batsman'] not in batsman.keys():
                    batsman[delivery['batsman']] = int(delivery['batsman_runs'])
                else:
                    batsman[delivery['batsman']] += int(delivery['batsman_runs'])
    return batsman                
                    
def get_top_scorer(batsman):   
    scores = []
    for score in batsman.values():
        scores.append(score)
    top_scores = sorted(scores)[::-1][:10]
    top_scorer = []   
    for i in range(len(top_scores)):
            top_scorer.append(list(batsman.keys())[list(batsman.values()).index(top_scores[i])])
    return top_scorer, top_scores

def plot(top_scorer, top_scores):
    plt.bar(top_scorer, top_scores, color="cyan")
    plt.xlabel("Name of batsman")
    plt.ylabel("scores")
    plt.xticks(rotation = 20, fontsize = 'x-small')
    plt.title("top scorer")
    plt.show()
    
def execute():
    season = input("input the year to know top scorer of that year in format 'yyyy'  ")
    batsman = get_scorer('deliveries.csv', ids_of_match_by_season.get_ids(season))
    top_scorer, top_scores = get_top_scorer(batsman)
    plot(top_scorer, top_scores) 
    
execute()    
