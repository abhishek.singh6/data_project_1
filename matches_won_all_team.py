import csv
import operator
from operator import add
from matplotlib import pyplot as plt

def matches_won(file_name):
    with open(file_name, newline ='') as csv_file:
        matches_reader = csv.DictReader(csv_file)
        matches_won_per_team_per_season = {}
        total_teams = []
        for match in matches_reader:
            if match['winner'] != '':
                if match['season'] not in matches_won_per_team_per_season:
                    matches_won_per_team_per_season[match['season']] = match['winner']
                else:
                    matches_won_per_team_per_season[match['season']] = match['winner'] +","+matches_won_per_team_per_season[match['season']]
                if match['winner'] not in total_teams:
                    total_teams.append(match['winner']) 
    return matches_won_per_team_per_season, total_teams   
      
def get_per_year_team_win_count(matches_won_per_team_per_season, total_teams):              
    total_teams.sort()
    per_year_team_win_count = {}
    for year in matches_won_per_team_per_season:
        total_win_team = matches_won_per_team_per_season[year].split(',')
        team_and_count = {}
        total_team_played = []
        for team in total_win_team:
            if team not in team_and_count:
                team_and_count[team] = 1
                total_team_played.append(team)
            else:
                team_and_count[team] += 1
        not_played_team = list(set(total_teams)-set(total_team_played))
        val_not_played_by_team = {}
        for team in not_played_team:
            val_not_played_by_team[team] = 0
        team_and_count.update(val_not_played_by_team)
        sorted_team_value = sorted(team_and_count.items(), key = operator.itemgetter(0))
        per_year_team_win_count[year] = sorted_team_value
    return per_year_team_win_count, total_teams

def plot(per_year_team_win_count, total_teams):
    x_value = total_teams
    yvalue = list(range(10, 100, 10))
    bottom_result = [0]*14
    for year in per_year_team_win_count:
        val_of_season = per_year_team_win_count[year]
        value_list = []
        for value in val_of_season:
            value_list.append(value[1])
        plt.bar(x = x_value, height = value_list, label = year, width = 0.5,bottom = bottom_result)
        bottom_result_list = list(map(add, bottom_result, value_list))
        bottom_result = bottom_result_list
    plt.legend(bbox_to_anchor = (1.05, 1), ncol = 5, loc = 4, borderaxespad = 1)
    plt.xlabel("Teams")
    plt.ylabel("season ")
    plt.xticks(rotation = 20, fontsize = 'x-small')
    plt.yticks(yvalue)
    plt.title('matches won of all teams over all the years of IPL.')
    plt.show()
    
def execute():
    matches_won_per_team_per_season, total_teams = matches_won('matches.csv')
    per_year_team_win_count, total_teams = get_per_year_team_win_count(matches_won_per_team_per_season, total_teams)
    plot(per_year_team_win_count, total_teams)
    
execute()
