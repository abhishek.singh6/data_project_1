from csv import DictReader
from matplotlib import pyplot as plt
import ids_of_match_by_season

def get_economical_bowlers(file_path, ids_of_match_by_season):
    bowlers = {}
    with open(file_path, 'rt') as csv_file:
        deliveries = DictReader(csv_file)
        for delivery in deliveries:
            if delivery['match_id'] in ids_of_match_by_season:
                if delivery['bowler'] not in bowlers.keys():
                    bowlers[delivery['bowler']] = {}
                    bowlers[delivery['bowler']]['ball'] = 1
                    bowlers[delivery['bowler']]['run'] = int(delivery['total_runs'])
                else:
                    bowlers[delivery['bowler']]['ball'] += 1
                    bowlers[delivery['bowler']]['run'] += int(delivery['total_runs'])
    return bowlers
                
def get_most_economical_bowlers(bowlers):           
    bowlers_economy = {}
    economy_list = []
    for key, value in bowlers.items():
        bowlers_economy[key] = round(((value['run']/value['ball'])*6), 3)
        economy_list.append(round(((value['run']/value['ball'])*6), 3))
    most_economical_list = sorted(economy_list)[:10]
    most_economical_bowlers = []
    for i in range(len(most_economical_list)):
        most_economical_bowlers.append(list(bowlers_economy.keys())[list(bowlers_economy.values()).index(most_economical_list[i])])
    return most_economical_bowlers, most_economical_list

def plot(most_economical_bowlers, most_economical_list):
    plt.bar(most_economical_bowlers, most_economical_list, label="Economy", color="g")
    plt.legend()
    plt.xlabel("Name of bowlers")
    plt.ylabel("Economy")
    plt.xticks(rotation = 20, fontsize = 'x-small')
    plt.title("Economy of bowlers")
    plt.show()
    
def execute():
    economical_bowlers = get_economical_bowlers('deliveries.csv', ids_of_match_by_season.get_ids(2015))
    most_economical_bowlers, most_economical_list = get_most_economical_bowlers(economical_bowlers)
    plot(most_economical_bowlers, most_economical_list) 
    
execute()         
