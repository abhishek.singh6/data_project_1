from csv import DictReader

def get_ids(season):
    season = str(season)
    ids_of_matches = []
    with open('matches.csv', 'rt') as csv_file:
        matches_reader = DictReader(csv_file)
        for match in matches_reader:
            if match['season'] == season:
                ids_of_matches.append(match['id'])
    return ids_of_matches
