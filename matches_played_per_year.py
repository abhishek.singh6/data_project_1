from csv import DictReader
from matplotlib import pyplot as plt

def matches_per_season(file_path):
    matches_count_dict = {}
    with open(file_path, 'rt') as csv_file:
        matches = DictReader(csv_file)
        for match in matches:
            if match['season'] not in matches_count_dict.keys():
                matches_count_dict[match['season']] = 1   
            matches_count_dict[match['season']] += 1
    return matches_count_dict

def plot(matches_count_dict): 
    matches_per_season = [match_count for match_count in matches_count_dict.values()] 
    seasons = [season for season in matches_count_dict.keys()]
    plt.bar(seasons, matches_per_season, label="IPL matches", color="cyan")
    plt.legend()
    plt.xlabel("Seasons")
    plt.ylabel("No. of matches")
    plt.title("IPL Matches per seasons")
    plt.show()
    
def execute():
    plot(matches_per_season('matches.csv'))
    
execute()    
