# Data Project 1
---

#### Dependencies
1. Python (programming language)
2. Matplotlib (Matplotlib is a Python 2D plotting library)

IPL Dataset is taken from [kaggle.com](https://www.kaggle.com/manasgarg/ipl) that have Ball by ball 
details for all matches for all season.

### Problem Set
***
```
In this data assignment you will transform raw data from IPL into graphs that will convey some meaning / analysis. For each part of this assignment you will have 2 parts -

Code python functions that will transform the raw csv data into a data structure in a format suitable for plotting with matplotlib.

Generate the following plots ...

1. Plot the number of matches played per year of all the years in IPL.
2. Plot a stacked bar chart of matches won of all teams over all the years of IPL.
3. For the year 2016 plot the extra runs conceded per team.
4. For the year 2015 plot the top economical bowlers.

```

### Solution
***

## solution 1

![solution 1](https://i.ibb.co/d4vDM2y/Figure-1.png)



## solution 2

![solution 1](https://i.ibb.co/DMk31R6/Figure-2.png)



## solution 3

![solution 3](https://i.ibb.co/Trh8CV0/Figure-3.png)



## solution 4

![solution 4](https://i.ibb.co/qD76ydK/Figure-4.png)



## solution 5

![solution 5](https://i.ibb.co/yhx15QS/Figure-5.png)
















