from csv import DictReader
from matplotlib import pyplot as plt
import ids_of_match_by_season

def get_extra_runs_per_teams(file_path, ids_of_match_by_season):
    teams = {}
    with open(file_path,'rt')as csv_file:
        deliveries_reader = DictReader(csv_file)
        for delivery in deliveries_reader:
            if delivery['match_id'] in ids_of_match_by_season:
                if delivery['bowling_team'] not in teams.keys():
                    teams[delivery['bowling_team']] = int(delivery['extra_runs'])
                else:
                    teams[delivery['bowling_team']] = int(delivery['extra_runs'])+ int(teams[delivery['bowling_team']])
    extra_runs_per_teams = [extra_runs for extra_runs in teams.values()]
    teams_list = [team for team in teams.keys()]
    return extra_runs_per_teams, teams_list

def plot(extra_runs_per_teams, teams_list):
    xcol = teams_list
    ycol = extra_runs_per_teams
    plt.bar(xcol, ycol, color = "g")
    plt.legend()
    plt.xlabel("IPL teams")
    plt.ylabel("No. of extra runs")
    plt.xticks(rotation = 20, fontsize = 'x-small')
    plt.show()
    
def execute():
    extra_runs_per_teams, teams_list = get_extra_runs_per_teams('deliveries.csv', ids_of_match_by_season.get_ids(2016))
    plot(extra_runs_per_teams, teams_list)
    
execute()      
